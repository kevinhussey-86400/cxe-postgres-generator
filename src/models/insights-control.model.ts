import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'insights_control'}}
})
export class InsightsControl extends Entity {
  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"control_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  controlId: Number;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"control_value","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  controlValue: String;

  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 2,
    postgresql: {"columnName":"run_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  runId: Number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<InsightsControl>) {
    super(data);
  }
}

export interface InsightsControlRelations {
  // describe navigational properties here
}

export type InsightsControlWithRelations = InsightsControl & InsightsControlRelations;

import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'user_analytics'}}
})
export class UserAnalytics extends Entity {
  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  tenantId?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"user_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  userId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"user_logon_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  userLogonName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"email","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  email?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"mobile_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  mobileNumber?: String;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"created","dataType":"timestamp without time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  created?: Date;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"email_active_flag","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  emailActiveFlag?: Boolean;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"marketing_opt_in_flag","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  marketingOptInFlag?: Boolean;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"potential_mule_flag","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  potentialMuleFlag?: Boolean;

  @property({
    type: Number,
    required: true,
    scale: 0,
    postgresql: {"columnName":"logon_30_days_count","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  logon_30DaysCount: Number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<UserAnalytics>) {
    super(data);
  }
}

export interface UserAnalyticsRelations {
  // describe navigational properties here
}

export type UserAnalyticsWithRelations = UserAnalytics & UserAnalyticsRelations;

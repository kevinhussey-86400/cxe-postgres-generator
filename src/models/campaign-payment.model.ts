import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'campaign_payment'}}
})
export class CampaignPayment extends Entity {
  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  @property({
    type: String,
    required: true,
    id: 2,
    postgresql: {"columnName":"id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  id: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"customer_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  customerId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"account_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  accountId?: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"referral","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  referral: String;

  @property({
    type: Number,
    required: true,
    precision: 30,
    scale: 6,
    postgresql: {"columnName":"amount","dataType":"numeric","dataLength":null,"dataPrecision":30,"dataScale":6,"nullable":"NO"},
  })
  amount: Number;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"amount_currency","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  amountCurrency: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"credit_description","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  creditDescription?: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"status","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  status: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"details","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  details?: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"payment_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  paymentId: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  updated?: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"lock_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  lockId?: String;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"lock_expiration","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  lockExpiration?: Date;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<CampaignPayment>) {
    super(data);
  }
}

export interface CampaignPaymentRelations {
  // describe navigational properties here
}

export type CampaignPaymentWithRelations = CampaignPayment & CampaignPaymentRelations;

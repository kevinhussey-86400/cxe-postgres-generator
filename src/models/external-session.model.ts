import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'external_session'}}
})
export class ExternalSession extends Entity {
  @property({
    type: String,
    required: true,
    id: 2,
    postgresql: {"columnName":"user_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  userId: String;

  @property({
    type: String,
    required: true,
    id: 3,
    postgresql: {"columnName":"session_provider","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  sessionProvider: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"correlation_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  correlationId: String;

  @property({
    type: Number,
    required: true,
    scale: 0,
    postgresql: {"columnName":"duration_in_minutes","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  durationInMinutes: Number;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"session_created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  sessionCreated: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"session_expiry","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  sessionExpiry: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  updated: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"session_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  sessionId?: String;

  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<ExternalSession>) {
    super(data);
  }
}

export interface ExternalSessionRelations {
  // describe navigational properties here
}

export type ExternalSessionWithRelations = ExternalSession & ExternalSessionRelations;

import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'poc_cxe', table: 'non_resident_tax_detail'}
  }
})
export class NonResidentTaxDetail extends Entity {
  @property({
    type: String,
    required: true,
    id: 2,
    postgresql: {"columnName":"non_resident_tax_detail_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  nonResidentTaxDetailId: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"customer_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  customerId: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"country","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  country?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"identifier","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  identifier?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"tax_exemption_reason","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  taxExemptionReason?: String;

  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<NonResidentTaxDetail>) {
    super(data);
  }
}

export interface NonResidentTaxDetailRelations {
  // describe navigational properties here
}

export type NonResidentTaxDetailWithRelations = NonResidentTaxDetail & NonResidentTaxDetailRelations;

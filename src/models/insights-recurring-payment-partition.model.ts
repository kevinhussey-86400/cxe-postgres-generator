import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'poc_cxe', table: 'insights_recurring_payment_partition'}
  }
})
export class InsightsRecurringPaymentPartition extends Entity {
  @property({
    type: String,
    required: true,
    id: 1,
    postgresql: {"columnName":"account_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  accountId: String;

  @property({
    type: Number,
    required: true,
    scale: 0,
    postgresql: {"columnName":"partition_key","dataType":"bigint","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  partitionKey: Number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<InsightsRecurringPaymentPartition>) {
    super(data);
  }
}

export interface InsightsRecurringPaymentPartitionRelations {
  // describe navigational properties here
}

export type InsightsRecurringPaymentPartitionWithRelations = InsightsRecurringPaymentPartition & InsightsRecurringPaymentPartitionRelations;

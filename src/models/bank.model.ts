import {Entity, model, property} from '@loopback/repository';

@model({settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'bank'}}})
export class Bank extends Entity {
  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 2,
    postgresql: {
      'columnName': 'bank_id',
      'dataType': 'integer',
      'dataLength': null,
      'dataPrecision': null,
      'dataScale': 0,
      'nullable': 'NO',
    },
  })
  bankId: Number;

  @property({
    type: String,
    required: false,
    postgresql: {
      'columnName': 'bank_routing_address',
      'dataType': 'text',
      'dataLength': null,
      'dataPrecision': null,
      'dataScale': null,
      'nullable': 'YES',
    },
  })
  bankRoutingAddress?: String;

  @property({
    type: String,
    required: false,
    postgresql: {
      'columnName': 'bank_routing_scheme',
      'dataType': 'text',
      'dataLength': null,
      'dataPrecision': null,
      'dataScale': null,
      'nullable': 'YES',
    },
  })
  bankRoutingScheme?: String;

  @property({
    type: String,
    required: false,
    postgresql: {
      'columnName': 'external_bank_id',
      'dataType': 'text',
      'dataLength': null,
      'dataPrecision': null,
      'dataScale': null,
      'nullable': 'YES',
    },
  })
  externalBankId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {
      'columnName': 'full_bank_name',
      'dataType': 'text',
      'dataLength': null,
      'dataPrecision': null,
      'dataScale': null,
      'nullable': 'YES',
    },
  })
  fullBankName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {
      'columnName': 'logo_url',
      'dataType': 'text',
      'dataLength': null,
      'dataPrecision': null,
      'dataScale': null,
      'nullable': 'YES',
    },
  })
  logoUrl?: String;

  @property({
    type: String,
    required: false,
    postgresql: {
      'columnName': 'national_identifier',
      'dataType': 'text',
      'dataLength': null,
      'dataPrecision': null,
      'dataScale': null,
      'nullable': 'YES',
    },
  })
  nationalIdentifier?: String;

  @property({
    type: String,
    required: false,
    postgresql: {
      'columnName': 'permalink',
      'dataType': 'text',
      'dataLength': null,
      'dataPrecision': null,
      'dataScale': null,
      'nullable': 'YES',
    },
  })
  permalink?: String;

  @property({
    type: String,
    required: false,
    postgresql: {
      'columnName': 'short_bank_name',
      'dataType': 'text',
      'dataLength': null,
      'dataPrecision': null,
      'dataScale': null,
      'nullable': 'YES',
    },
  })
  shortBankName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {
      'columnName': 'swift_bic',
      'dataType': 'text',
      'dataLength': null,
      'dataPrecision': null,
      'dataScale': null,
      'nullable': 'YES',
    },
  })
  swiftBic?: String;

  @property({
    type: String,
    required: false,
    postgresql: {
      'columnName': 'website_url',
      'dataType': 'text',
      'dataLength': null,
      'dataPrecision': null,
      'dataScale': null,
      'nullable': 'YES',
    },
  })
  websiteUrl?: String;

  @property({
    type: Boolean,
    required: false,
    postgresql: {
      'columnName': 'is_enabled',
      'dataType': 'boolean',
      'dataLength': null,
      'dataPrecision': null,
      'dataScale': null,
      'nullable': 'YES',
    },
  })
  isEnabled?: Boolean;

  @property({
    type: Boolean,
    required: false,
    postgresql: {
      'columnName': 'is_popular',
      'dataType': 'boolean',
      'dataLength': null,
      'dataPrecision': null,
      'dataScale': null,
      'nullable': 'YES',
    },
  })
  isPopular?: Boolean;

  @property({
    type: Date,
    required: true,
    postgresql: {
      'columnName': 'created',
      'dataType': 'timestamp with time zone',
      'dataLength': null,
      'dataPrecision': null,
      'dataScale': null,
      'nullable': 'NO',
    },
  })
  created: Date;

  @property({
    type: Date,
    required: false,
    postgresql: {
      'columnName': 'updated',
      'dataType': 'timestamp with time zone',
      'dataLength': null,
      'dataPrecision': null,
      'dataScale': null,
      'nullable': 'YES',
    },
  })
  updated?: Date;

  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {
      'columnName': 'tenant_id',
      'dataType': 'integer',
      'dataLength': null,
      'dataPrecision': null,
      'dataScale': 0,
      'nullable': 'NO',
    },
  })
  tenantId: Number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Bank>) {
    super(data);
  }
}

export interface BankRelations {
  // describe navigational properties here
}

export type BankWithRelations = Bank & BankRelations;

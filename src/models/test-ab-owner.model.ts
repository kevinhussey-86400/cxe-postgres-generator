import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'test_ab_owner'}}
})
export class TestAbOwner extends Entity {
  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"mobile_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  mobileNumber: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"email_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  emailId: String;

  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"owner_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  ownerId: Number;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"date_of_birth","dataType":"date","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  dateOfBirth: Date;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  name: String;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<TestAbOwner>) {
    super(data);
  }
}

export interface TestAbOwnerRelations {
  // describe navigational properties here
}

export type TestAbOwnerWithRelations = TestAbOwner & TestAbOwnerRelations;

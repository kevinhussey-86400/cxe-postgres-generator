import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'poc_cxe', table: 'flyway_schema_history'}
  }
})
export class FlywaySchemaHistory extends Entity {
  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"installed_rank","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  installedRank: Number;

  @property({
    type: String,
    required: false,
    length: 50,
    postgresql: {"columnName":"version","dataType":"character varying","dataLength":50,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  version?: String;

  @property({
    type: String,
    required: true,
    length: 200,
    postgresql: {"columnName":"description","dataType":"character varying","dataLength":200,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  description: String;

  @property({
    type: String,
    required: true,
    length: 20,
    postgresql: {"columnName":"type","dataType":"character varying","dataLength":20,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  type: String;

  @property({
    type: String,
    required: true,
    length: 1000,
    postgresql: {"columnName":"script","dataType":"character varying","dataLength":1000,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  script: String;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"checksum","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  checksum?: Number;

  @property({
    type: String,
    required: true,
    length: 100,
    postgresql: {"columnName":"installed_by","dataType":"character varying","dataLength":100,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  installedBy: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"installed_on","dataType":"timestamp without time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  installedOn: Date;

  @property({
    type: Number,
    required: true,
    scale: 0,
    postgresql: {"columnName":"execution_time","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  executionTime: Number;

  @property({
    type: Boolean,
    required: true,
    postgresql: {"columnName":"success","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  success: Boolean;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<FlywaySchemaHistory>) {
    super(data);
  }
}

export interface FlywaySchemaHistoryRelations {
  // describe navigational properties here
}

export type FlywaySchemaHistoryWithRelations = FlywaySchemaHistory & FlywaySchemaHistoryRelations;

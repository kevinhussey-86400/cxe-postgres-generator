import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'poc_cxe', table: 'add_device_failed_attempts'}
  }
})
export class AddDeviceFailedAttempts extends Entity {
  @property({
    type: String,
    required: true,
    id: 2,
    postgresql: {"columnName":"user_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  userId: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"user_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  userName: String;

  @property({
    type: Number,
    required: true,
    scale: 0,
    postgresql: {"columnName":"pin_failed_count","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  pinFailedCount: Number;

  @property({
    type: Number,
    required: true,
    scale: 0,
    postgresql: {"columnName":"customer_data_failed_count","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  customerDataFailedCount: Number;

  @property({
    type: Number,
    required: true,
    scale: 0,
    postgresql: {"columnName":"duration","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  duration: Number;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  updated: Date;

  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<AddDeviceFailedAttempts>) {
    super(data);
  }
}

export interface AddDeviceFailedAttemptsRelations {
  // describe navigational properties here
}

export type AddDeviceFailedAttemptsWithRelations = AddDeviceFailedAttempts & AddDeviceFailedAttemptsRelations;

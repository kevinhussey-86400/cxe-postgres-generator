import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'kyc_enquiry'}}
})
export class KycEnquiry extends Entity {
  @property({
    type: String,
    required: true,
    id: 2,
    postgresql: {"columnName":"kyc_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  kycId: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"correlation_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  correlationId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"first_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  firstName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"last_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  lastName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"middle_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  middleName?: String;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"date_of_birth","dataType":"date","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  dateOfBirth?: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"api_response","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  apiResponse?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"provider_response","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  providerResponse?: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  updated: Date;

  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<KycEnquiry>) {
    super(data);
  }
}

export interface KycEnquiryRelations {
  // describe navigational properties here
}

export type KycEnquiryWithRelations = KycEnquiry & KycEnquiryRelations;

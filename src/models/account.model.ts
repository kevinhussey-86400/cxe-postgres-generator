import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'account'}}
})
export class Account extends Entity {
  @property({
    type: String,
    required: true,
    id: 2,
    postgresql: {"columnName":"account_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  accountId: String;

  @property({
    type: Number,
    required: true,
    scale: 0,
    postgresql: {"columnName":"bank_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  bankId: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"opened_date","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  openedDate?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"key_hash","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  keyHash?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"last_summary_refresh","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  lastSummaryRefresh?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"account_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  accountType?: String;

  @property({
    type: Number,
    required: false,
    postgresql: {"columnName":"current_balance","dataType":"numeric","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  currentBalance?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"currency","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  currency?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"label","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  label?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"account_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  accountNumber?: String;

  @property({
    type: Number,
    required: false,
    postgresql: {"columnName":"available_balance","dataType":"numeric","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  availableBalance?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"status","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  status?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"branch","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  branch?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"db_state","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  dbState?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"routing_address","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  routingAddress?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"routing_scheme","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  routingScheme?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"tnc_accepted_at","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  tncAcceptedAt?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"closed_date","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  closedDate?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"last_details_refresh","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  lastDetailsRefresh?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"last_transactions_refresh","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  lastTransactionsRefresh?: String;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"has_redraw","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  hasRedraw?: Boolean;

  @property({
    type: Number,
    required: false,
    postgresql: {"columnName":"available_for_redraw_amount","dataType":"numeric","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  availableForRedrawAmount?: Number;

  @property({
    type: Number,
    required: false,
    postgresql: {"columnName":"contract_amount","dataType":"numeric","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  contractAmount?: Number;

  @property({
    type: Number,
    required: false,
    postgresql: {"columnName":"in_advance_amount","dataType":"numeric","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  inAdvanceAmount?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"in_advance_until_date","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  inAdvanceUntilDate?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"last_bill_status","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  lastBillStatus?: String;

  @property({
    type: Number,
    required: false,
    postgresql: {"columnName":"last_repayment_amount","dataType":"numeric","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  lastRepaymentAmount?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"last_repayment_date","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  lastRepaymentDate?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"loan_purpose","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  loanPurpose?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"maturity_date","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  maturityDate?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"next_repayment_date","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  nextRepaymentDate?: String;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"number_of_times_delinquent","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  numberOfTimesDelinquent?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"offset_to","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  offsetTo?: String;

  @property({
    type: Number,
    required: false,
    postgresql: {"columnName":"on_hold_amount","dataType":"numeric","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  onHoldAmount?: Number;

  @property({
    type: Number,
    required: false,
    postgresql: {"columnName":"overdue_amount","dataType":"numeric","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  overdueAmount?: Number;

  @property({
    type: Number,
    required: false,
    postgresql: {"columnName":"payoff_amount","dataType":"numeric","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payoffAmount?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"period","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  period?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"rate_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  rateType?: String;

  @property({
    type: Number,
    required: false,
    postgresql: {"columnName":"repayment_amount","dataType":"numeric","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  repaymentAmount?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"repayment_period","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  repaymentPeriod?: String;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"repayment_term","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  repaymentTerm?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"repayment_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  repaymentType?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"settlement_date","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  settlementDate?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"statement_frequency","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  statementFrequency?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"term","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  term?: String;

  @property({
    type: Number,
    required: false,
    postgresql: {"columnName":"uncollected_amount","dataType":"numeric","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  uncollectedAmount?: Number;

  @property({
    type: Number,
    required: false,
    postgresql: {"columnName":"debit_interest_current_rate","dataType":"numeric","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  debitInterestCurrentRate?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"debit_interest_fixed_rate_expiry_date","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  debitInterestFixedRateExpiryDate?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"debit_interest_fixed_rate_period","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  debitInterestFixedRatePeriod?: String;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"debit_interest_fixed_rate_term","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  debitInterestFixedRateTerm?: Number;

  @property({
    type: Number,
    required: false,
    postgresql: {"columnName":"debit_interest_interest_accrued","dataType":"numeric","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  debitInterestInterestAccrued?: Number;

  @property({
    type: Number,
    required: false,
    postgresql: {"columnName":"debit_interest_interest_paid_last_year","dataType":"numeric","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  debitInterestInterestPaidLastYear?: Number;

  @property({
    type: Number,
    required: false,
    postgresql: {"columnName":"debit_interest_interest_paid_year_to_date","dataType":"numeric","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  debitInterestInterestPaidYearToDate?: Number;

  @property({
    type: Number,
    required: false,
    postgresql: {"columnName":"debit_interest_margin","dataType":"numeric","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  debitInterestMargin?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"debit_interest_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  debitInterestType?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"credit_interest_account_base_rate","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  creditInterestAccountBaseRate?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"credit_interest_product_base_rate","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  creditInterestProductBaseRate?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"credit_interest_current_rate","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  creditInterestCurrentRate?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"credit_interest_interest_accrued","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  creditInterestInterestAccrued?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"credit_interest_interest_paid_last_year","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  creditInterestInterestPaidLastYear?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"credit_interest_interest_paid_year_to_date","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  creditInterestInterestPaidYearToDate?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"credit_interest_payment_method","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  creditInterestPaymentMethod?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"credit_interest_linked_account","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  creditInterestLinkedAccount?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"external_payment_account_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  externalPaymentAccountNumber?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"external_payment_account_title","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  externalPaymentAccountTitle?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"external_payment_bsb","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  externalPaymentBsb?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"external_payment_full_bank_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  externalPaymentFullBankName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"internal_payment_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  internalPaymentType?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"internal_payment_account_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  internalPaymentAccountNumber?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"linked_account_account_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  linkedAccountAccountNumber?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"linked_account_branch","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  linkedAccountBranch?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"external_data_last_updated","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  externalDataLastUpdated?: String;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"external_data_external_account_status_code","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  externalDataExternalAccountStatusCode?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"external_data_external_account_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  externalDataExternalAccountId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"external_data_maturity_date","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  externalDataMaturityDate?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"external_data_provider_account_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  externalDataProviderAccountId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"external_data_last_update_attempt","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  externalDataLastUpdateAttempt?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"external_data_provider_account_status","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  externalDataProviderAccountStatus?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"external_data_external_account_status_message","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  externalDataExternalAccountStatusMessage?: String;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"external_data_provider_account_status_code","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  externalDataProviderAccountStatusCode?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"external_data_annual_percentage_yield","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  externalDataAnnualPercentageYield?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"external_data_classification","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  externalDataClassification?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"external_data_maturity_amount","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  externalDataMaturityAmount?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"external_data_term","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  externalDataTerm?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"external_data_memo","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  externalDataMemo?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"external_data_over_draft_limit","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  externalDataOverDraftLimit?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"external_data_include_in_net_worth","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  externalDataIncludeInNetWorth?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"external_data_provider_account_additional_status","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  externalDataProviderAccountAdditionalStatus?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"external_data_provider_account_status_message","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  externalDataProviderAccountStatusMessage?: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  updated: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"external_customer_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  externalCustomerId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"nickname","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  nickname?: String;

  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"close_account_ticket_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  closeAccountTicketNumber?: String;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Account>) {
    super(data);
  }
}

export interface AccountRelations {
  // describe navigational properties here
}

export type AccountWithRelations = Account & AccountRelations;

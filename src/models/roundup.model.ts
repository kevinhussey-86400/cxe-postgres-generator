import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'roundup'}}
})
export class Roundup extends Entity {
  @property({
    type: String,
    required: true,
    id: 1,
    postgresql: {"columnName":"user_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  userId: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"from_account","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  fromAccount?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"to_account","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  toAccount?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"roundup","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  roundup?: String;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Roundup>) {
    super(data);
  }
}

export interface RoundupRelations {
  // describe navigational properties here
}

export type RoundupWithRelations = Roundup & RoundupRelations;

import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'address'}}
})
export class Address extends Entity {
  @property({
    type: String,
    required: true,
    id: 2,
    postgresql: {"columnName":"address_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  addressId: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"customer_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  customerId: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"address_format","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  addressFormat?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"address_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  addressType?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"flat_or_box_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  flatOrBoxNumber?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"postcode","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  postcode?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"state","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  state?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"street_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  streetName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"street_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  streetNumber?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"street_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  streetType?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"suburb","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  suburb?: String;

  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Address>) {
    super(data);
  }
}

export interface AddressRelations {
  // describe navigational properties here
}

export type AddressWithRelations = Address & AddressRelations;

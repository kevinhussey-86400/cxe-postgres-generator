import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'poc_cxe', table: 'statement_file_da'}
  }
})
export class StatementFileDa extends Entity {
  @property({
    type: Number,
    required: true,
    scale: 0,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  @property({
    type: String,
    required: true,
    id: 1,
    postgresql: {"columnName":"statement_file_da_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  statementFileDaId: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"file_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  fileName: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"processing_date","dataType":"date","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  processingDate: Date;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"file_md5","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  fileMd5: String;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"version_number","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  versionNumber?: Number;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"system_date_time","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  systemDateTime?: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"bank_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  bankName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"bank_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  bankNumber?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"reserved_space","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  reservedSpace?: String;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"number_of_statements","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  numberOfStatements?: Number;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"number_of_statement_lines","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  numberOfStatementLines?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"processing_status","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  processingStatus?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"processing_errors","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  processingErrors?: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  updated: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<StatementFileDa>) {
    super(data);
  }
}

export interface StatementFileDaRelations {
  // describe navigational properties here
}

export type StatementFileDaWithRelations = StatementFileDa & StatementFileDaRelations;

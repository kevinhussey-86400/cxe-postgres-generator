import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'cxe_user_dupl'}}
})
export class CxeUserDupl extends Entity {
  @property({
    type: String,
    required: true,
    id: 1,
    postgresql: {"columnName":"user_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  userId: String;

  @property({
    type: Number,
    required: true,
    scale: 0,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"auth_provider_user_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  authProviderUserId: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  name: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"email","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  email: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"mobile_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  mobileNumber: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"tnc_accepted_at","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  tncAcceptedAt?: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  updated: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<CxeUserDupl>) {
    super(data);
  }
}

export interface CxeUserDuplRelations {
  // describe navigational properties here
}

export type CxeUserDuplWithRelations = CxeUserDupl & CxeUserDuplRelations;

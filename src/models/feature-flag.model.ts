import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'feature_flag'}}
})
export class FeatureFlag extends Entity {
  @property({
    type: String,
    required: true,
    id: 1,
    postgresql: {"columnName":"entity_key","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  entityKey: String;

  @property({
    type: String,
    required: true,
    id: 2,
    postgresql: {"columnName":"entity_value","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  entityValue: String;

  @property({
    type: String,
    required: true,
    id: 3,
    postgresql: {"columnName":"feature_category","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  featureCategory: String;

  @property({
    type: String,
    required: true,
    id: 4,
    postgresql: {"columnName":"feature_key","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  featureKey: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"feature_value","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  featureValue: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  updated: Date;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"is_regex","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  isRegex?: Boolean;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"feature_description","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  featureDescription?: String;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<FeatureFlag>) {
    super(data);
  }
}

export interface FeatureFlagRelations {
  // describe navigational properties here
}

export type FeatureFlagWithRelations = FeatureFlag & FeatureFlagRelations;

import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'application'}}
})
export class Application extends Entity {
  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  @property({
    type: String,
    required: true,
    id: 2,
    postgresql: {"columnName":"application_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  applicationId: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"application_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  applicationType: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"application_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  applicationNumber: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"application_data","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  applicationData: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"primary_customer_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  primaryCustomerId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"secondary_customer_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  secondaryCustomerId?: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  updated: Date;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"status","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  status: String;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Application>) {
    super(data);
  }
}

export interface ApplicationRelations {
  // describe navigational properties here
}

export type ApplicationWithRelations = Application & ApplicationRelations;

import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'lookup'}}
})
export class Lookup extends Entity {
  @property({
    type: String,
    required: true,
    id: 1,
    postgresql: {"columnName":"category","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  category: String;

  @property({
    type: String,
    required: true,
    id: 2,
    postgresql: {"columnName":"type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  type: String;

  @property({
    type: String,
    required: true,
    id: 3,
    postgresql: {"columnName":"key","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  key: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"value","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  value: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  updated: Date;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Lookup>) {
    super(data);
  }
}

export interface LookupRelations {
  // describe navigational properties here
}

export type LookupWithRelations = Lookup & LookupRelations;

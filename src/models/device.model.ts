import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'device'}}
})
export class Device extends Entity {
  @property({
    type: String,
    required: true,
    id: 1,
    postgresql: {"columnName":"device_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  deviceId: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"user_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  userId: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"push_token","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  pushToken?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"token_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  tokenType?: String;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"push_enabled","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  pushEnabled?: Boolean;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"push_deposit_withdrawal","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  pushDepositWithdrawal?: Boolean;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  updated: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"hardware","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  hardware?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"model","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  model?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"software","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  software?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"software_version","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  softwareVersion?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"app_binary_version","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  appBinaryVersion?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"app_version","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  appVersion?: String;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"deleted","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  deleted?: Date;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"push_outages","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  pushOutages?: Boolean;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"push_bonus_interest","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  pushBonusInterest?: Boolean;

  @property({
    type: Number,
    required: true,
    scale: 0,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"push_banking_insights","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  pushBankingInsights?: Boolean;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Device>) {
    super(data);
  }
}

export interface DeviceRelations {
  // describe navigational properties here
}

export type DeviceWithRelations = Device & DeviceRelations;

import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'audit_event'}}
})
export class AuditEvent extends Entity {
  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  @property({
    type: String,
    required: true,
    id: 2,
    postgresql: {"columnName":"audit_event_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  auditEventId: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  type: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"entity_key","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  entityKey: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"entity_value","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  entityValue: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"metadata","dataType":"jsonb","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  metadata: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"timestamp","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  timestamp: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<AuditEvent>) {
    super(data);
  }
}

export interface AuditEventRelations {
  // describe navigational properties here
}

export type AuditEventWithRelations = AuditEvent & AuditEventRelations;

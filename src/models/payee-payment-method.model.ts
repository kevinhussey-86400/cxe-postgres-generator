import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'poc_cxe', table: 'payee_payment_method'}
  }
})
export class PayeePaymentMethod extends Entity {
  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  @property({
    type: String,
    required: true,
    id: 2,
    postgresql: {"columnName":"payee_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  payeeId: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"address_book_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  addressBookId: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"payee_u_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  payeeUType: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payee_account_u_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payeeAccountUType?: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"status","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  status: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"account_bsb","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  accountBsb?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"account_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  accountName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"account_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  accountNumber?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"pay_id_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payIdName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"pay_id_identifier","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payIdIdentifier?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"pay_id_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payIdType?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"biller_code","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  billerCode?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"biller_crn","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  billerCrn?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"biller_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  billerName?: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  updated?: Date;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<PayeePaymentMethod>) {
    super(data);
  }
}

export interface PayeePaymentMethodRelations {
  // describe navigational properties here
}

export type PayeePaymentMethodWithRelations = PayeePaymentMethod & PayeePaymentMethodRelations;

import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'poc_cxe', table: 'incomplete_npp_payments'}
  }
})
export class IncompleteNppPayments extends Entity {
  @property({
    type: String,
    required: true,
    id: 2,
    postgresql: {"columnName":"user_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  userId: String;

  @property({
    type: String,
    required: true,
    id: 3,
    postgresql: {"columnName":"payment_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  paymentId: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"status","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  status: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"customer_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  customerNumber: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"from_account_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  fromAccountNumber?: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  updated: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<IncompleteNppPayments>) {
    super(data);
  }
}

export interface IncompleteNppPaymentsRelations {
  // describe navigational properties here
}

export type IncompleteNppPaymentsWithRelations = IncompleteNppPayments & IncompleteNppPaymentsRelations;

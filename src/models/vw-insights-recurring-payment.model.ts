import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'poc_cxe', table: 'vw_insights_recurring_payment'}
  }
})
export class VwInsightsRecurringPayment extends Entity {
  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  tenantId?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"account_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  accountId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"recurring_payment_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  recurringPaymentId?: String;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"run_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  runId?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"account_label","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  accountLabel?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_category","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantCategory?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"frequency","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  frequency?: String;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"frequency_unit","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  frequencyUnit?: Number;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"predicted_date","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  predictedDate?: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"predicted_amount_currency","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  predictedAmountCurrency?: String;

  @property({
    type: Number,
    required: false,
    precision: 30,
    scale: 6,
    postgresql: {"columnName":"predicted_amount_value","dataType":"numeric","dataLength":null,"dataPrecision":30,"dataScale":6,"nullable":"YES"},
  })
  predictedAmountValue?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"confidence_level","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  confidenceLevel?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"recurring_payment_model","dataType":"jsonb","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  recurringPaymentModel?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"transaction_data","dataType":"jsonb","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  transactionData?: String;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  created?: Date;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<VwInsightsRecurringPayment>) {
    super(data);
  }
}

export interface VwInsightsRecurringPaymentRelations {
  // describe navigational properties here
}

export type VwInsightsRecurringPaymentWithRelations = VwInsightsRecurringPayment & VwInsightsRecurringPaymentRelations;

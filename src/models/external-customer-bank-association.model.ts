import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'poc_cxe', table: 'external_customer_bank_association'}
  }
})
export class ExternalCustomerBankAssociation extends Entity {
  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 2,
    postgresql: {"columnName":"bank_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  bankId: Number;

  @property({
    type: String,
    required: true,
    id: 3,
    postgresql: {"columnName":"external_customer_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  externalCustomerId: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  updated: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<ExternalCustomerBankAssociation>) {
    super(data);
  }
}

export interface ExternalCustomerBankAssociationRelations {
  // describe navigational properties here
}

export type ExternalCustomerBankAssociationWithRelations = ExternalCustomerBankAssociation & ExternalCustomerBankAssociationRelations;

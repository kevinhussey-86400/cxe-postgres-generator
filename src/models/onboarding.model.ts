import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'onboarding'}}
})
export class Onboarding extends Entity {
  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  @property({
    type: String,
    required: true,
    id: 2,
    postgresql: {"columnName":"onboarding_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  onboardingId: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"origin","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  origin: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"reference_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  referenceNumber: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"username","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  username: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"mobile_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  mobileNumber: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"request_history_json","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  requestHistoryJson: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"user_status","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  userStatus: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"user_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  userId?: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"kyc_status","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  kycStatus: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"kyc_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  kycId?: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"customer_status","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  customerStatus: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"customer_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  customerId?: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"application_status","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  applicationStatus: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"application_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  applicationId?: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"pay_account_status","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  payAccountStatus: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"pay_account_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payAccountId?: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"save_account_status","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  saveAccountStatus: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"save_account_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  saveAccountId?: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"welcome_email_status","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  welcomeEmailStatus: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  updated: Date;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"user_uuid","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  userUuid: String;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Onboarding>) {
    super(data);
  }
}

export interface OnboardingRelations {
  // describe navigational properties here
}

export type OnboardingWithRelations = Onboarding & OnboardingRelations;

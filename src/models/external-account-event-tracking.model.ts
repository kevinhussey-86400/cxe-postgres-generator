import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'poc_cxe', table: 'external_account_event_tracking'}
  }
})
export class ExternalAccountEventTracking extends Entity {
  @property({
    type: String,
    required: true,
    id: 2,
    postgresql: {"columnName":"job_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  jobType: String;

  @property({
    type: String,
    required: true,
    id: 3,
    postgresql: {"columnName":"service_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  serviceId: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"correlation_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  correlationId: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"last_from","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  lastFrom: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"last_to","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  lastTo: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  updated: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<ExternalAccountEventTracking>) {
    super(data);
  }
}

export interface ExternalAccountEventTrackingRelations {
  // describe navigational properties here
}

export type ExternalAccountEventTrackingWithRelations = ExternalAccountEventTracking & ExternalAccountEventTrackingRelations;

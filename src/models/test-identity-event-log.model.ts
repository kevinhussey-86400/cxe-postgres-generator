import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'poc_cxe', table: 'test_identity_event_log'}
  }
})
export class TestIdentityEventLog extends Entity {
  @property({
    type: String,
    required: true,
    id: 1,
    postgresql: {"columnName":"event_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  eventId: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"identifier","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  identifier: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"discriminator","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  discriminator: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"status","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  status: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"username","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  username?: String;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<TestIdentityEventLog>) {
    super(data);
  }
}

export interface TestIdentityEventLogRelations {
  // describe navigational properties here
}

export type TestIdentityEventLogWithRelations = TestIdentityEventLog & TestIdentityEventLogRelations;

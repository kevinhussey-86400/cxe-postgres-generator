import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'campaign'}}
})
export class Campaign extends Entity {
  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  @property({
    type: String,
    required: true,
    id: 2,
    postgresql: {"columnName":"id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  id: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  name?: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"start_date","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  startDate: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"end_date","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  endDate: Date;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"referrals_max_number","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  referralsMaxNumber?: Number;

  @property({
    type: Boolean,
    required: true,
    postgresql: {"columnName":"is_active","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  isActive: Boolean;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"campaign_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  campaignType: String;

  @property({
    type: Number,
    required: false,
    precision: 30,
    scale: 6,
    postgresql: {"columnName":"referrer_amount","dataType":"numeric","dataLength":null,"dataPrecision":30,"dataScale":6,"nullable":"YES"},
  })
  referrerAmount?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"referrer_amount_currency","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  referrerAmountCurrency?: String;

  @property({
    type: Number,
    required: false,
    precision: 30,
    scale: 6,
    postgresql: {"columnName":"referred_amount","dataType":"numeric","dataLength":null,"dataPrecision":30,"dataScale":6,"nullable":"YES"},
  })
  referredAmount?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"referred_amount_currency","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  referredAmountCurrency?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"terms_and_conditions_uri","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  termsAndConditionsUri?: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  updated?: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"referred_credit_description","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  referredCreditDescription?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"referrer_credit_description","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  referrerCreditDescription?: String;

  @property({
    type: Number,
    required: true,
    scale: 0,
    postgresql: {"columnName":"referrals_valid_days","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  referralsValidDays: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"hurdle","dataType":"jsonb","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  hurdle?: String;

  @property({
    type: Number,
    required: true,
    scale: 0,
    postgresql: {"columnName":"priority","dataType":"smallint","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  priority: Number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Campaign>) {
    super(data);
  }
}

export interface CampaignRelations {
  // describe navigational properties here
}

export type CampaignWithRelations = Campaign & CampaignRelations;

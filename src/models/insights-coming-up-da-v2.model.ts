import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'poc_cxe', table: 'insights_coming_up_da_v2'}
  }
})
export class InsightsComingUpDaV2 extends Entity {
  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  tenantId?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"bank_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  bankId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"short_bank_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  shortBankName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"account_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  accountId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"currency","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  currency?: String;

  @property({
    type: Number,
    required: false,
    precision: 30,
    scale: 6,
    postgresql: {"columnName":"amount","dataType":"numeric","dataLength":null,"dataPrecision":30,"dataScale":6,"nullable":"YES"},
  })
  amount?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"description","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  description?: String;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"date","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  date?: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"external_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  externalId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"account_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  accountType?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"account_label","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  accountLabel?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"lwc_merchant_category","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  lwcMerchantCategory?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"lwc_merchant_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  lwcMerchantName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"lwc_square_logo","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  lwcSquareLogo?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"lwc_circular_logo","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  lwcCircularLogo?: String;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<InsightsComingUpDaV2>) {
    super(data);
  }
}

export interface InsightsComingUpDaV2Relations {
  // describe navigational properties here
}

export type InsightsComingUpDaV2WithRelations = InsightsComingUpDaV2 & InsightsComingUpDaV2Relations;

import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'customer_code'}}
})
export class CustomerCode extends Entity {
  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"code","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  code?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"customer_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  customerId?: String;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<CustomerCode>) {
    super(data);
  }
}

export interface CustomerCodeRelations {
  // describe navigational properties here
}

export type CustomerCodeWithRelations = CustomerCode & CustomerCodeRelations;

import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'kyc_equifax_tmp'}}
})
export class KycEquifaxTmp extends Entity {
  @property({
    type: String,
    required: true,
    id: 1,
    postgresql: {"columnName":"enquiry_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  enquiryId: String;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"created","dataType":"timestamp without time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  created?: Date;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<KycEquifaxTmp>) {
    super(data);
  }
}

export interface KycEquifaxTmpRelations {
  // describe navigational properties here
}

export type KycEquifaxTmpWithRelations = KycEquifaxTmp & KycEquifaxTmpRelations;

import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'card_mocked'}}
})
export class CardMocked extends Entity {
  @property({
    type: String,
    required: true,
    id: 2,
    postgresql: {"columnName":"card_number_sequence","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  cardNumberSequence: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"member_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  memberNumber?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"account_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  accountNumber?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"card_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  cardNumber?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"card_number_masked","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  cardNumberMasked?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"switch_token","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  switchToken?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"status","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  status?: String;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"expiry_date","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  expiryDate?: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"pin","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  pin?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"pan_reference_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  panReferenceId?: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  updated?: Date;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"card_control_15","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  cardControl_15?: Boolean;

  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<CardMocked>) {
    super(data);
  }
}

export interface CardMockedRelations {
  // describe navigational properties here
}

export type CardMockedWithRelations = CardMocked & CardMockedRelations;

import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'audit'}}
})
export class Audit extends Entity {
  @property({
    type: String,
    required: true,
    id: 2,
    postgresql: {"columnName":"audit_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  auditId: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"primary_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  primaryId: String;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"audit_timestamp","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  auditTimestamp?: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"audit_event_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  auditEventType?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"audit_text","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  auditText?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"source","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  source?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"correlation_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  correlationId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"audit_user_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  auditUserId?: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  updated: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Audit>) {
    super(data);
  }
}

export interface AuditRelations {
  // describe navigational properties here
}

export type AuditWithRelations = Audit & AuditRelations;

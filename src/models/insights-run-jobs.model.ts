import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'poc_cxe', table: 'insights_run_jobs'}
  }
})
export class InsightsRunJobs extends Entity {
  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"run_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  runId: Number;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"job_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  jobName: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"job_version","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  jobVersion?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"status","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  status?: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"start_time","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  startTime: Date;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"end_time","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  endTime?: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"status_description","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  statusDescription?: String;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<InsightsRunJobs>) {
    super(data);
  }
}

export interface InsightsRunJobsRelations {
  // describe navigational properties here
}

export type InsightsRunJobsWithRelations = InsightsRunJobs & InsightsRunJobsRelations;

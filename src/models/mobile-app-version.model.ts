import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'poc_cxe', table: 'mobile_app_version'}
  }
})
export class MobileAppVersion extends Entity {
  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  id: Number;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"live_app_version","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  liveAppVersion: String;

  @property({
    type: Boolean,
    required: true,
    postgresql: {"columnName":"force_rollback","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  forceRollback: Boolean;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"locked_versions","dataType":"jsonb","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  lockedVersions: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"invalid_app_versions","dataType":"jsonb","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  invalidAppVersions: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"invalid_binary_versions","dataType":"jsonb","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  invalidBinaryVersions: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"min_binary_version","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  minBinaryVersion: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"force_min_binary_version","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  forceMinBinaryVersion: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  updated: Date;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<MobileAppVersion>) {
    super(data);
  }
}

export interface MobileAppVersionRelations {
  // describe navigational properties here
}

export type MobileAppVersionWithRelations = MobileAppVersion & MobileAppVersionRelations;

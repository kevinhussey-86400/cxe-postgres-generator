import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'poc_cxe', table: 'test_ab_car_owner'}
  }
})
export class TestAbCarOwner extends Entity {
  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"car_owner_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  carOwnerId: Number;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"owner_id","dataType":"character varying","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  ownerId: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"car_id","dataType":"character varying","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  carId: String;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<TestAbCarOwner>) {
    super(data);
  }
}

export interface TestAbCarOwnerRelations {
  // describe navigational properties here
}

export type TestAbCarOwnerWithRelations = TestAbCarOwner & TestAbCarOwnerRelations;

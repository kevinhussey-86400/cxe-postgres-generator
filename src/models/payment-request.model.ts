import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'payment_request'}}
})
export class PaymentRequest extends Entity {
  @property({
    type: String,
    required: true,
    id: 2,
    postgresql: {"columnName":"payment_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  paymentId: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"account_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  accountId?: String;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"bank_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  bankId?: Number;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"request_time","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  requestTime?: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payment_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  paymentType?: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  updated: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"user_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  userId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"device_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  deviceId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"correlation_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  correlationId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"geocode_latitude","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  geocodeLatitude?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"geocode_longitude","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  geocodeLongitude?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"dest_account_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  destAccountId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"dest_bsb","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  destBsb?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"dest_account_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  destAccountNumber?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"dest_account_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  destAccountName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"dest_flan","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  destFlan?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payment_description","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  paymentDescription?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payment_reference","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  paymentReference?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"dest_biller_code","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  destBillerCode?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"dest_biller_crn","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  destBillerCrn?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"dest_pay_id_identifier","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  destPayIdIdentifier?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"dest_pay_id_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  destPayIdType?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"dest_pay_id_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  destPayIdName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payment_scheme","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  paymentScheme?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"npp_service_overlay","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  nppServiceOverlay?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"amount_currency","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  amountCurrency?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"amount","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  amount?: String;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"response_time","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  responseTime?: Date;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"http_status_code","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  httpStatusCode?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"http_status_reason","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  httpStatusReason?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"response_body","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  responseBody?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"credit_receipt_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  creditReceiptNumber?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"debit_receipt_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  debitReceiptNumber?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payment_status","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  paymentStatus?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"reject_reason_code","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  rejectReasonCode?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"reject_reason_message","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  rejectReasonMessage?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"npp_transaction_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  nppTransactionId?: String;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"npp_start_date","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  nppStartDate?: Date;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"npp_end_date","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  nppEndDate?: Date;

  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"request_body","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  requestBody?: String;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<PaymentRequest>) {
    super(data);
  }
}

export interface PaymentRequestRelations {
  // describe navigational properties here
}

export type PaymentRequestWithRelations = PaymentRequest & PaymentRequestRelations;

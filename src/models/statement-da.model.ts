import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'statement_da'}}
})
export class StatementDa extends Entity {
  @property({
    type: Number,
    required: true,
    scale: 0,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  @property({
    type: String,
    required: true,
    id: 1,
    postgresql: {"columnName":"statement_da_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  statementDaId: String;

  @property({
    type: Number,
    required: true,
    scale: 0,
    postgresql: {"columnName":"statement_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  statementId: Number;

  @property({
    type: Number,
    required: true,
    scale: 0,
    postgresql: {"columnName":"customer_number","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  customerNumber: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"customer_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  customerName?: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"period_start_date","dataType":"date","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  periodStartDate: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"period_end_date","dataType":"date","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  periodEndDate: Date;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"statement_number","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  statementNumber?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"one_off_statement","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  oneOffStatement?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"financial_year_statement","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  financialYearStatement?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"rim_charge_category","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  rimChargeCategory?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"electronic_statement_email_address","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  electronicStatementEmailAddress?: String;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"number_of_accounts","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  numberOfAccounts?: Number;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"line_start","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  lineStart?: Number;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"line_end","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  lineEnd?: Number;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"statement_file_da_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  statementFileDaId: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  updated: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<StatementDa>) {
    super(data);
  }
}

export interface StatementDaRelations {
  // describe navigational properties here
}

export type StatementDaWithRelations = StatementDa & StatementDaRelations;

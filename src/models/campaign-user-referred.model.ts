import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'poc_cxe', table: 'campaign_user_referred'}
  }
})
export class CampaignUserReferred extends Entity {
  @property({
    type: String,
    required: true,
    id: 2,
    postgresql: {"columnName":"id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  id: String;

  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"campaign_invite_code","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  campaignInviteCode: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"username","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  username: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"claimed_date","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  claimedDate: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"referred_payment","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  referredPayment?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"referrer_payment","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  referrerPayment?: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  updated?: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"campaign_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  campaignId?: String;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<CampaignUserReferred>) {
    super(data);
  }
}

export interface CampaignUserReferredRelations {
  // describe navigational properties here
}

export type CampaignUserReferredWithRelations = CampaignUserReferred & CampaignUserReferredRelations;

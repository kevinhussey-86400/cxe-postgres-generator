import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'poc_cxe', table: 'customer_analytics'}
  }
})
export class CustomerAnalytics extends Entity {
  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"user_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  userId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"user_logon_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  userLogonName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"email","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  email?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"mobile_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  mobileNumber?: String;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"created","dataType":"timestamp without time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  created?: Date;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"logon_30_days","dataType":"bigint","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  logon_30Days?: Number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<CustomerAnalytics>) {
    super(data);
  }
}

export interface CustomerAnalyticsRelations {
  // describe navigational properties here
}

export type CustomerAnalyticsWithRelations = CustomerAnalytics & CustomerAnalyticsRelations;

import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'lwc_cal'}}
})
export class LwcCal extends Entity {
  @property({
    type: String,
    required: true,
    id: 1,
    postgresql: {"columnName":"cal_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  calId: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"cal_description","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  calDescription: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"acquirer_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  acquirerId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"terminal_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  terminalId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"bpay_biller_code","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  bpayBillerCode?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"direct_entry_code","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  directEntryCode?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_guid","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantGuid?: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  updated: Date;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"transaction_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  transactionType: String;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<LwcCal>) {
    super(data);
  }
}

export interface LwcCalRelations {
  // describe navigational properties here
}

export type LwcCalWithRelations = LwcCal & LwcCalRelations;

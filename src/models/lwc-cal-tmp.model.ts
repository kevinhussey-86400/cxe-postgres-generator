import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'lwc_cal_tmp'}}
})
export class LwcCalTmp extends Entity {
  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"cal_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  calId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"cal_description","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  calDescription?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"acquirer_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  acquirerId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"terminal_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  terminalId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"bpay_biller_code","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  bpayBillerCode?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"direct_entry_code","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  directEntryCode?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_guid","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantGuid?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"transaction_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  transactionType?: String;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"created","dataType":"timestamp without time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  created?: Date;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"updated","dataType":"timestamp without time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  updated?: Date;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<LwcCalTmp>) {
    super(data);
  }
}

export interface LwcCalTmpRelations {
  // describe navigational properties here
}

export type LwcCalTmpWithRelations = LwcCalTmp & LwcCalTmpRelations;

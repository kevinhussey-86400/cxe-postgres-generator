import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'lwc_merchant'}}
})
export class LwcMerchant extends Entity {
  @property({
    type: String,
    required: true,
    id: 1,
    postgresql: {"columnName":"merchant_guid","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  merchantGuid: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"merchant_json","dataType":"jsonb","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  merchantJson: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_location","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantLocation?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_category","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantCategory?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_light_logo_square_url","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantLightLogoSquareUrl?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_light_logo_circular_url","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantLightLogoCircularUrl?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_dark_logo_square_url","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantDarkLogoSquareUrl?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_dark_logo_circular_url","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantDarkLogoCircularUrl?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_primary_address","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantPrimaryAddress?: String;

  @property({
    type: Number,
    required: false,
    postgresql: {"columnName":"merchant_primary_address_longitude","dataType":"numeric","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantPrimaryAddressLongitude?: Number;

  @property({
    type: Number,
    required: false,
    postgresql: {"columnName":"merchant_primary_address_latitude","dataType":"numeric","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantPrimaryAddressLatitude?: Number;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"merchant_primary_address_mappable","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantPrimaryAddressMappable?: Boolean;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_primary_contact_website","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantPrimaryContactWebsite?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_primary_contact_phone","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantPrimaryContactPhone?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_primary_contact_email","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantPrimaryContactEmail?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_primary_category_full_path","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantPrimaryCategoryFullPath?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_abn","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantAbn?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantType?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_presence","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantPresence?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_chain_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantChainName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_current_status","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantCurrentStatus?: String;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"merchant_suspicious_score","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  merchantSuspiciousScore?: Number;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"registered_for_sales_tax","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  registeredForSalesTax?: Boolean;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"is_sensitive","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  isSensitive?: Boolean;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  updated: Date;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"deleted","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  deleted?: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_internal_primary_category","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantInternalPrimaryCategory?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_internal_secondary_category","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantInternalSecondaryCategory?: String;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<LwcMerchant>) {
    super(data);
  }
}

export interface LwcMerchantRelations {
  // describe navigational properties here
}

export type LwcMerchantWithRelations = LwcMerchant & LwcMerchantRelations;

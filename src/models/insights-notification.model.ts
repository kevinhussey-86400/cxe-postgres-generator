import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'poc_cxe', table: 'insights_notification'}
  }
})
export class InsightsNotification extends Entity {
  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 2,
    postgresql: {"columnName":"id","dataType":"bigint","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  id: Number;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"account_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  accountId: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"notification_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  notificationId: String;

  @property({
    type: Number,
    required: true,
    scale: 0,
    postgresql: {"columnName":"notification_counter","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  notificationCounter: Number;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  updated: Date;

  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"notification_data","dataType":"jsonb","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  notificationData?: String;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<InsightsNotification>) {
    super(data);
  }
}

export interface InsightsNotificationRelations {
  // describe navigational properties here
}

export type InsightsNotificationWithRelations = InsightsNotification & InsightsNotificationRelations;

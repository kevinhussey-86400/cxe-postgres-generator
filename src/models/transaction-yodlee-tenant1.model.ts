import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'poc_cxe', table: 'transaction_yodlee_tenant_1'}
  }
})
export class TransactionYodleeTenant_1 extends Entity {
  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"bank_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  bankId?: String;

  @property({
    type: String,
    required: true,
    id: 2,
    postgresql: {"columnName":"account_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  accountId: String;

  @property({
    type: Binary,
    required: true,
    id: 3,
    postgresql: {"columnName":"id","dataType":"bytea","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  id: Binary;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"effective_ts","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  effectiveTs: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"amount_currency","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  amountCurrency?: String;

  @property({
    type: Number,
    required: false,
    precision: 30,
    scale: 6,
    postgresql: {"columnName":"amount","dataType":"numeric","dataLength":null,"dataPrecision":30,"dataScale":6,"nullable":"YES"},
  })
  amount?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"display_description","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  displayDescription?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"external_account_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  externalAccountId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"base_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  baseType?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"category","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  category?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"container","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  container?: String;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"transaction_date","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  transactionDate?: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"external_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  externalId?: String;

  @property({
    type: Number,
    required: false,
    precision: 30,
    scale: 6,
    postgresql: {"columnName":"interest","dataType":"numeric","dataLength":null,"dataPrecision":30,"dataScale":6,"nullable":"YES"},
  })
  interest?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"interest_currency","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  interestCurrency?: String;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"is_manual","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  isManual?: Boolean;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"memo","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  memo?: String;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"post_date","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  postDate?: Date;

  @property({
    type: Number,
    required: false,
    precision: 30,
    scale: 6,
    postgresql: {"columnName":"principal","dataType":"numeric","dataLength":null,"dataPrecision":30,"dataScale":6,"nullable":"YES"},
  })
  principal?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"principal_currency","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  principalCurrency?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"status","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  status?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"transaction_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  transactionType?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"description_original","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  descriptionOriginal?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"description_customer","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  descriptionCustomer?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"description_simple","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  descriptionSimple?: String;

  @property({
    type: Number,
    required: false,
    precision: 30,
    scale: 6,
    postgresql: {"columnName":"balance","dataType":"numeric","dataLength":null,"dataPrecision":30,"dataScale":6,"nullable":"YES"},
  })
  balance?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"balance_currency","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  balanceCurrency?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"parent_category_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  parentCategoryId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"sub_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  subType?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_source","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantSource?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_category_label","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantCategoryLabel?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_address","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantAddress?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_coordinates","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantCoordinates?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"category_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  categoryType?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"category_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  categoryId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"category_source","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  categorySource?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"high_level_category_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  highLevelCategoryId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"check_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  checkNumber?: String;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"created_date","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  createdDate?: Date;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"last_updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  lastUpdated?: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"source_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  sourceType?: String;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"is_deleted","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  isDeleted?: Boolean;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  updated?: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"short_display_description","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  shortDisplayDescription?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"long_description","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  longDescription?: String;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<TransactionYodleeTenant_1>) {
    super(data);
  }
}

export interface TransactionYodleeTenant_1Relations {
  // describe navigational properties here
}

export type TransactionYodleeTenant_1WithRelations = TransactionYodleeTenant_1 & TransactionYodleeTenant_1Relations;

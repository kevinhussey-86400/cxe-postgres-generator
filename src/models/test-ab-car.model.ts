import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'test_ab_car'}}
})
export class TestAbCar extends Entity {
  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"car_model","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  carModel: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"car_manufacturing_year","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  carManufacturingYear: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"car_make","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  carMake: String;

  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"car_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  carId: Number;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"ownerOwnerId","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  ownerOwnerId?: Number;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"deleteAt","dataType":"timestamp without time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  deleteAt?: Date;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"version","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  version?: Number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<TestAbCar>) {
    super(data);
  }
}

export interface TestAbCarRelations {
  // describe navigational properties here
}

export type TestAbCarWithRelations = TestAbCar & TestAbCarRelations;

import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'savings_goal'}}
})
export class SavingsGoal extends Entity {
  @property({
    type: String,
    required: true,
    id: 1,
    postgresql: {"columnName":"goal_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  goalId: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"account_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  accountId: String;

  @property({
    type: Number,
    required: true,
    postgresql: {"columnName":"amount","dataType":"numeric","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  amount: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"date_to_achieve","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  dateToAchieve?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"frequency","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  frequency?: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"goal_status","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  goalStatus: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"created_by_customer","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  createdByCustomer: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"updated_by_customer","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  updatedByCustomer?: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  updated?: Date;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"date_first_completed","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  dateFirstCompleted?: Date;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<SavingsGoal>) {
    super(data);
  }
}

export interface SavingsGoalRelations {
  // describe navigational properties here
}

export type SavingsGoalWithRelations = SavingsGoal & SavingsGoalRelations;

import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'poc_cxe', table: 'transaction_da_pending_archive'}
  }
})
export class TransactionDaPendingArchive extends Entity {
  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  @property({
    type: String,
    required: true,
    id: 2,
    postgresql: {"columnName":"account_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  accountId: String;

  @property({
    type: Binary,
    required: true,
    id: 3,
    postgresql: {"columnName":"id","dataType":"bytea","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  id: Binary;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"effective_ts","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  effectiveTs: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"bank_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  bankId?: String;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"posted_ts","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  postedTs?: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"display_description","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  displayDescription?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"category","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  category?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"amount_currency","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  amountCurrency?: String;

  @property({
    type: Number,
    required: false,
    precision: 30,
    scale: 6,
    postgresql: {"columnName":"amount","dataType":"numeric","dataLength":null,"dataPrecision":30,"dataScale":6,"nullable":"YES"},
  })
  amount?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"narrational_currency","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  narrationalCurrency?: String;

  @property({
    type: Number,
    required: false,
    precision: 30,
    scale: 6,
    postgresql: {"columnName":"narrational_amount","dataType":"numeric","dataLength":null,"dataPrecision":30,"dataScale":6,"nullable":"YES"},
  })
  narrationalAmount?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"balance_currency","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  balanceCurrency?: String;

  @property({
    type: Number,
    required: false,
    precision: 30,
    scale: 6,
    postgresql: {"columnName":"balance","dataType":"numeric","dataLength":null,"dataPrecision":30,"dataScale":6,"nullable":"YES"},
  })
  balance?: Number;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"is_deleted","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  isDeleted?: Boolean;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  updated?: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"unique_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  uniqueId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"transaction_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  transactionId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"posting_code","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  postingCode?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"reversal","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  reversal?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"receipt_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  receiptNumber?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"wallet_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  walletType?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"card_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  cardNumber?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"transaction_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  transactionType?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payment_scheme","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  paymentScheme?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"npp_transaction_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  nppTransactionId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"npp_mapped_service_overlay","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  nppMappedServiceOverlay?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"atm_location","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  atmLocation?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"batch_identifier","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  batchIdentifier?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"acquirer_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  acquirerId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"acquirer_institution_country","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  acquirerInstitutionCountry?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"bpay_biller_code","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  bpayBillerCode?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"channel_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  channelId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"card_acceptor_code","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  cardAcceptorCode?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"cheque_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  chequeNumber?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"credit_receipt_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  creditReceiptNumber?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"debit_or_credit","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  debitOrCredit?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"debit_receipt_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  debitReceiptNumber?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"description","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  description?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"direct_charge_amount","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  directChargeAmount?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"direct_entry_tran_code","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  directEntryTranCode?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"employee_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  employeeId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"foreign_currency","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  foreignCurrency?: String;

  @property({
    type: Number,
    required: false,
    precision: 30,
    scale: 6,
    postgresql: {"columnName":"foreign_amount","dataType":"numeric","dataLength":null,"dataPrecision":30,"dataScale":6,"nullable":"YES"},
  })
  foreignAmount?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"internal_table_key","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  internalTableKey?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"internal_table_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  internalTableName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"item_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  itemType?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"long_description","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  longDescription?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"member_category_code","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  memberCategoryCode?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_category_code","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantCategoryCode?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"origin_identifier","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  originIdentifier?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"origin_trace_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  originTraceNumber?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"pos_condition_code","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  posConditionCode?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"pos_entry_mode","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  posEntryMode?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payment_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  paymentId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"reference_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  referenceNumber?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"retrieval_reference_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  retrievalReferenceNumber?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"system_trace_audit_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  systemTraceAuditNumber?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"settlement_date","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  settlementDate?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"supervisor_employee_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  supervisorEmployeeId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"terminal_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  terminalId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"visa_terminal_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  visaTerminalId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"wallet_indicator","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  walletIndicator?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"type_code","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  typeCode?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"type_description","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  typeDescription?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"type_group","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  typeGroup?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payee_account_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payeeAccountName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payee_account_description","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payeeAccountDescription?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payee_account_legal_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payeeAccountLegalName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payee_account_bsb","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payeeAccountBsb?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payee_account_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payeeAccountNumber?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payee_pay_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payeePayId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payee_pay_id_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payeePayIdName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payee_pay_id_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payeePayIdType?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payee_bpay_biller_code","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payeeBpayBillerCode?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payee_bpay_biller_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payeeBpayBillerName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payee_bpay_biller_nickname","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payeeBpayBillerNickname?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payee_bpay_crn","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payeeBpayCrn?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payer_account_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payerAccountName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payer_account_description","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payerAccountDescription?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payer_account_legal_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payerAccountLegalName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payer_account_bsb","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payerAccountBsb?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payer_account_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payerAccountNumber?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payer_pay_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payerPayId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payer_pay_id_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payerPayIdName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payer_pay_id_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payerPayIdType?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"short_display_description","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  shortDisplayDescription?: String;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<TransactionDaPendingArchive>) {
    super(data);
  }
}

export interface TransactionDaPendingArchiveRelations {
  // describe navigational properties here
}

export type TransactionDaPendingArchiveWithRelations = TransactionDaPendingArchive & TransactionDaPendingArchiveRelations;

import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'poc_cxe', table: 'insights_savings_graph'}
  }
})
export class InsightsSavingsGraph extends Entity {
  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  @property({
    type: Number,
    required: true,
    scale: 0,
    postgresql: {"columnName":"customer_number","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  customerNumber: Number;

  @property({
    type: String,
    required: true,
    id: 2,
    postgresql: {"columnName":"account_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  accountNumber: String;

  @property({
    type: Date,
    required: true,
    id: 3,
    postgresql: {"columnName":"effective_date","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  effectiveDate: Date;

  @property({
    type: Number,
    required: true,
    postgresql: {"columnName":"current_balance","dataType":"numeric","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  currentBalance: Number;

  @property({
    type: Number,
    required: false,
    postgresql: {"columnName":"forecast_bal","dataType":"numeric","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  forecastBal?: Number;

  @property({
    type: Number,
    required: false,
    postgresql: {"columnName":"trend_lower","dataType":"numeric","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  trendLower?: Number;

  @property({
    type: Number,
    required: false,
    postgresql: {"columnName":"trend_upper","dataType":"numeric","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  trendUpper?: Number;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<InsightsSavingsGraph>) {
    super(data);
  }
}

export interface InsightsSavingsGraphRelations {
  // describe navigational properties here
}

export type InsightsSavingsGraphWithRelations = InsightsSavingsGraph & InsightsSavingsGraphRelations;

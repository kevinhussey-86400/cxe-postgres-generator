import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'customer'}}
})
export class Customer extends Entity {
  @property({
    type: String,
    required: true,
    id: 2,
    postgresql: {"columnName":"customer_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  customerId: String;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"customer_number","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  customerNumber?: Number;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"user_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  userId: String;

  @property({
    type: Number,
    required: true,
    scale: 0,
    postgresql: {"columnName":"bank_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  bankId: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"key_hash","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  keyHash?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"db_state","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  dbState?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"kyc_cra_result","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  kycCraResult?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"kyc_fraud_assessment_overall_result","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  kycFraudAssessmentOverallResult?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"kyc_fraud_assessment_pep_result","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  kycFraudAssessmentPepResult?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"kyc_fraud_assessment_sanctions_result","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  kycFraudAssessmentSanctionsResult?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"kyc_fraud_assessment_velocity_result","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  kycFraudAssessmentVelocityResult?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"kyc_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  kycId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"kyc_overall_outcome","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  kycOverallOutcome?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"kyc_verification_outcome","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  kycVerificationOutcome?: String;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"kyc_status","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  kycStatus?: Boolean;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"tax_exemption_reason","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  taxExemptionReason?: String;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"are_tax_details_provided","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  areTaxDetailsProvided?: Boolean;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"title","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  title?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"first_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  firstName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"middle_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  middleName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"last_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  lastName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"date_of_birth","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  dateOfBirth?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"gender","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  gender?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"country_of_residence","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  countryOfResidence?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"mobile_phone_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  mobilePhoneNumber?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"email","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  email?: String;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"is_welcome_email_sent","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  isWelcomeEmailSent?: Boolean;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"marketing_opt_in","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  marketingOptIn?: Boolean;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  updated: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"origin","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  origin?: String;

  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Customer>) {
    super(data);
  }
}

export interface CustomerRelations {
  // describe navigational properties here
}

export type CustomerWithRelations = Customer & CustomerRelations;

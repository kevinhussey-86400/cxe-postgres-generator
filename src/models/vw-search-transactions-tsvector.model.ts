import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'poc_cxe', table: 'vw_search_transactions_tsvector'}
  }
})
export class VwSearchTransactionsTsvector extends Entity {
  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  type?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"display_description","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  displayDescription?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"short_display_description","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  shortDisplayDescription?: String;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"completed","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  completed?: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"description","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  description?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"long_description","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  longDescription?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payee_account_description","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payeeAccountDescription?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payer_account_description","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payerAccountDescription?: String;

  @property({
    type: Number,
    required: false,
    precision: 30,
    scale: 6,
    postgresql: {"columnName":"amount","dataType":"numeric","dataLength":null,"dataPrecision":30,"dataScale":6,"nullable":"YES"},
  })
  amount?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"transaction_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  transactionType?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"cal_description","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  calDescription?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_name_alt","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantNameAlt?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_location","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantLocation?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"account_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  accountId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"search_field","dataType":"tsvector","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  searchField?: String;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<VwSearchTransactionsTsvector>) {
    super(data);
  }
}

export interface VwSearchTransactionsTsvectorRelations {
  // describe navigational properties here
}

export type VwSearchTransactionsTsvectorWithRelations = VwSearchTransactionsTsvector & VwSearchTransactionsTsvectorRelations;

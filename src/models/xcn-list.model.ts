import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'xcn_list'}}
})
export class XcnList extends Entity {
  @property({
    type: Number,
    required: true,
    scale: 0,
    postgresql: {"columnName":"id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  id: Number;

  @property({
    type: Number,
    required: true,
    scale: 0,
    postgresql: {"columnName":"base_xcn","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  baseXcn: Number;

  @property({
    type: Number,
    required: true,
    scale: 0,
    postgresql: {"columnName":"check_digit","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  checkDigit: Number;

  @property({
    type: Number,
    required: true,
    scale: 0,
    postgresql: {"columnName":"xcn","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  xcn: Number;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"used","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  used?: Date;

  @property({
    type: Number,
    required: true,
    scale: 0,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<XcnList>) {
    super(data);
  }
}

export interface XcnListRelations {
  // describe navigational properties here
}

export type XcnListWithRelations = XcnList & XcnListRelations;

import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'lookup_bsb'}}
})
export class LookupBsb extends Entity {
  @property({
    type: String,
    required: true,
    id: 1,
    postgresql: {"columnName":"bsb","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  bsb: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"bank","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  bank?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"bank_and_branch","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  bankAndBranch?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"branch","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  branch?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"source","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  source?: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  updated: Date;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<LookupBsb>) {
    super(data);
  }
}

export interface LookupBsbRelations {
  // describe navigational properties here
}

export type LookupBsbWithRelations = LookupBsb & LookupBsbRelations;

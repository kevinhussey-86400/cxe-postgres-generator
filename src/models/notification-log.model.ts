import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'notification_log'}}
})
export class NotificationLog extends Entity {
  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 2,
    postgresql: {"columnName":"id","dataType":"bigint","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  id: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"user_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  userId?: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"notification_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  notificationId: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"device_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  deviceId?: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"sent","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  sent: Date;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  type: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"phone_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  phoneNumber?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"email_address","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  emailAddress?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"title","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  title?: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"message","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  message: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  updated: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"service_response_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  serviceResponseId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"source","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  source?: String;

  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<NotificationLog>) {
    super(data);
  }
}

export interface NotificationLogRelations {
  // describe navigational properties here
}

export type NotificationLogWithRelations = NotificationLog & NotificationLogRelations;

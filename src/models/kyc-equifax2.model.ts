import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'poc_cxe', table: 'kyc_equifax2'}}
})
export class KycEquifax2 extends Entity {
  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"kyc_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  kycId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"correlation_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  correlationId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"crm_case_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  crmCaseId?: String;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"date_of_birth","dataType":"date","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  dateOfBirth?: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"email","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  email?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"first_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  firstName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"last_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  lastName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"middle_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  middleName?: String;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"is_sms_sent","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  isSmsSent?: Boolean;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"mobile_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  mobileNumber?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"original_result_cra","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  originalResultCra?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"original_result_dvs","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  originalResultDvs?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"original_result_fraud_overall","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  originalResultFraudOverall?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"original_result_overall","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  originalResultOverall?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"original_result_pep","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  originalResultPep?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"original_result_sanction","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  originalResultSanction?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"original_result_velocity","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  originalResultVelocity?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"original_result_verification","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  originalResultVerification?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"overridden_result_overall","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  overriddenResultOverall?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"overridden_result_pep","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  overriddenResultPep?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"overridden_result_sanction","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  overriddenResultSanction?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"overridden_result_verification","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  overriddenResultVerification?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payload","dataType":"jsonb","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payload?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payload_hash","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  payloadHash?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"search_results","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  searchResults?: String;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  created?: Date;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  updated?: Date;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<KycEquifax2>) {
    super(data);
  }
}

export interface KycEquifax2Relations {
  // describe navigational properties here
}

export type KycEquifax2WithRelations = KycEquifax2 & KycEquifax2Relations;

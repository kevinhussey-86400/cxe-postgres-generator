import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'poc_cxe', table: 'lwc_merchant_details_tmp'}
  }
})
export class LwcMerchantDetailsTmp extends Entity {
  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"lwc_guid","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  lwcGuid?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"primary_category_json","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  primaryCategoryJson?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"primary_address_json","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  primaryAddressJson?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"primary_contacts_json","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  primaryContactsJson?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"verification_json","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  verificationJson?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_primary_name","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantPrimaryName?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"primary_merchant_description","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  primaryMerchantDescription?: String;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"suspicious_merchant_score","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  suspiciousMerchantScore?: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"secondary_contacts_json","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  secondaryContactsJson?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"secondary_addresses_json","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  secondaryAddressesJson?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"image_gallery_json","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  imageGalleryJson?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"other_categories_list_json","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  otherCategoriesListJson?: String;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"is_permanently_closed","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  isPermanentlyClosed?: Boolean;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_logo_json","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantLogoJson?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_logo_3x1_json","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantLogo_3x1Json?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_logo_circular_json","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantLogoCircularJson?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_logo_dark_json","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantLogoDarkJson?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_logo_3x1_dark_json","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantLogo_3x1DarkJson?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"merchant_logo_circular_dark_json","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  merchantLogoCircularDarkJson?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"website_screen_shot_json","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  websiteScreenShotJson?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"also_known_as_json","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  alsoKnownAsJson?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"associated_with_json","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  associatedWithJson?: String;

  @property({
    type: Date,
    required: false,
    postgresql: {"columnName":"last_updated","dataType":"timestamp without time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  lastUpdated?: Date;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"opening_hours_json","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  openingHoursJson?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"legal_business_info_json","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  legalBusinessInfoJson?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"overall_rating_json","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  overallRatingJson?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"ratings_json","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  ratingsJson?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"payment_options_json","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  paymentOptionsJson?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"transaction_query_process_json","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  transactionQueryProcessJson?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"transaction_query_process_html","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  transactionQueryProcessHtml?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"transaction_query_process_as_json","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  transactionQueryProcessAsJson?: String;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"receipt_data_available","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  receiptDataAvailable?: Boolean;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"record_is_quarantined","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  recordIsQuarantined?: Boolean;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"additional_details_json","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  additionalDetailsJson?: String;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<LwcMerchantDetailsTmp>) {
    super(data);
  }
}

export interface LwcMerchantDetailsTmpRelations {
  // describe navigational properties here
}

export type LwcMerchantDetailsTmpWithRelations = LwcMerchantDetailsTmp & LwcMerchantDetailsTmpRelations;

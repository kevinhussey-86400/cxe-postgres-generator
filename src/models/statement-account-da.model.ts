import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'poc_cxe', table: 'statement_account_da'}
  }
})
export class StatementAccountDa extends Entity {
  @property({
    type: Number,
    required: true,
    scale: 0,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  @property({
    type: String,
    required: true,
    id: 1,
    postgresql: {"columnName":"statement_account_da_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  statementAccountDaId: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"account_number","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  accountNumber: String;

  @property({
    type: Number,
    required: true,
    scale: 0,
    postgresql: {"columnName":"customer_number","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  customerNumber: Number;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"account_sequence","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  accountSequence?: Number;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"account_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  accountType: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"account_description","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  accountDescription?: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"account_data","dataType":"json","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  accountData: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"period_start_date","dataType":"date","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  periodStartDate: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"period_end_date","dataType":"date","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  periodEndDate: Date;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"line_start","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  lineStart?: Number;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"line_end","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  lineEnd?: Number;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"statement_da_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  statementDaId: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  updated: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<StatementAccountDa>) {
    super(data);
  }
}

export interface StatementAccountDaRelations {
  // describe navigational properties here
}

export type StatementAccountDaWithRelations = StatementAccountDa & StatementAccountDaRelations;

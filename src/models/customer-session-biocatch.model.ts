import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'poc_cxe', table: 'customer_session_biocatch'}
  }
})
export class CustomerSessionBiocatch extends Entity {
  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"tenant_id","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  tenantId: Number;

  @property({
    type: String,
    required: true,
    id: 2,
    postgresql: {"columnName":"request_id","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  requestId: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"user_id","dataType":"uuid","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  userId?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"aid","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  aid?: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"csid","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  csid: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"activity_type","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  activityType: String;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"created","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  created: Date;

  @property({
    type: Date,
    required: true,
    postgresql: {"columnName":"updated","dataType":"timestamp with time zone","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  updated: Date;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"action","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  action: String;

  @property({
    type: String,
    required: true,
    postgresql: {"columnName":"status","dataType":"text","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"NO"},
  })
  status: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"request_body","dataType":"jsonb","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  requestBody?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"response_body","dataType":"jsonb","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  responseBody?: String;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: {"columnName":"score","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"YES"},
  })
  score?: Number;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"is_bot","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  isBot?: Boolean;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"is_emulator","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  isEmulator?: Boolean;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"is_rat","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  isRat?: Boolean;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"is_recent_rat","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  isRecentRat?: Boolean;

  @property({
    type: Boolean,
    required: false,
    postgresql: {"columnName":"is_mobile_rat","dataType":"boolean","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  isMobileRat?: Boolean;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<CustomerSessionBiocatch>) {
    super(data);
  }
}

export interface CustomerSessionBiocatchRelations {
  // describe navigational properties here
}

export type CustomerSessionBiocatchWithRelations = CustomerSessionBiocatch & CustomerSessionBiocatchRelations;

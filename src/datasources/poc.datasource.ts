import {inject} from '@loopback/core';
import {juggler} from '@loopback/repository';
import * as config from './poc.datasource.json';

export class PocDataSource extends juggler.DataSource {
  static dataSourceName = 'poc';

  constructor(
    @inject('datasources.config.poc', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}

Prerequisites : Be able to connect to postgres poc (vpm & user/password)

Edit src/datasource (poc) too include the user and password, root has access to all tables.

To build a Model/Entity from a DB schema run 
```npm run clean && npm run build && lb4 discover --dataSource poc --schema poc_cxe ```

If the model has a dual PK e.g. customerId & tenantId to run the discover relations (one to many, belongs to etc)
change id: 1/2 to id: true 

